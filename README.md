My dotfiles <3. I keep it simple stupid and very minimal.

* Hardware: [Acer C720](http://a.co/b6lg7tR)
* OS: [Arch Linux](https://www.archlinux.org/)
* Window Manager: [bspwm](https://github.com/baskerville/bspwm)
* Terminal: [urxvt](https://wiki.archlinux.org/index.php/Rxvt-unicode) ([rxvt-unicode-patched](https://aur.archlinux.org/packages/rxvt-unicode-patched/))
* Shell: [bash](https://www.gnu.org/software/bash/)
* Font: [Source Code Pro](https://github.com/adobe-fonts/source-code-pro)
* Text Editor: [vim](https://www.vim.org/)
* Web Browser: [Firefox](https://www.mozilla.org/en-US/firefox/new/)
    * [Stylish](https://addons.mozilla.org/en-US/firefox/addon/stylish/)
    * [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
    * [NoScript](https://addons.mozilla.org/en-US/firefox/addon/noscript/)
    * [Imagus](https://addons.mozilla.org/en-US/firefox/addon/imagus/)
* Bar: [lemonbar](https://github.com/LemonBoy/bar) ([lemonbar-xft-git](https://aur.archlinux.org/packages/lemonbar-xft-git/))
* Launcher: [dmenu](https://wiki.archlinux.org/index.php/Dmenu)
* Background: [nitrogen](https://wiki.archlinux.org/index.php/Nitrogen)

![scrot](scrot.png)
